module.exports = {
    plugins: [
      require("tailwindcss")("tailwind.config.js"),
      require("autoprefixer")({
        overrideBrowserslist: ["> 0.5% in US", "Safari > 9"]
      })
    ]
  };
  